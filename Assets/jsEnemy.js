﻿#pragma strict

private var power = 1200;

var bullet : Transform;
var target : Transform;
var spPoint : Transform;
var Rcast : Transform;
var explosion : Transform;
var snd : AudioClip;

private var ftime : float = 0.0;	// 추가

function Start () {

}

function Update () {
	transform.LookAt(target);
	ftime += Time.deltaTime;	//추가
	
	var hit : RaycastHit;
	var fwd = transform.TransformDirection(Vector3.forward);
	
//	Debug.DrawRay(Rcast.transform.position, fwd * 30, Color.green);
	if(Physics.Raycast(Rcast.transform.position, fwd, hit, 30) == false) return;
	if (hit.collider.gameObject.tag != "TANK" || ftime < 2 ) return;	//추가
	
//	Debug.Log(hit.collider.gameObject.name);
	
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	
	var obj = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
	obj.rigidbody.AddForce(fwd * power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
	ftime = 0;
}