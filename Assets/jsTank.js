﻿#pragma strict
var speed = 15;
var rotSpeed = 120;
var rotSpeed1 = 12;
var turret : GameObject;
var turret1 : GameObject; //포탑과 따로 포신을 추가

var power = 2500;
var bullet : Transform;
var explosion : Transform;
var snd : AudioClip;

function Update () {
	var amtToMove = speed * Time.deltaTime;
	var amtToRot = rotSpeed * Time.deltaTime;
	var amtToRot1 = rotSpeed1 * Time.deltaTime; //포신 로테이트
	
	var front = Input.GetAxis("Vertical");
	var ang = Input.GetAxis("Horizontal");
	var ang2 = Input.GetAxis("MyTank"); //포탑 좌우 마우스이동
	var ang3 = Input.GetAxis("MyTank2"); //포신 상하각도조정 qe
	
	transform.Translate(Vector3.forward * front * amtToMove);
	transform.Rotate(Vector3(0,ang * amtToRot, 0));
	turret.transform.Rotate(Vector3.up * ang2 * amtToRot1);
	
	//각도 한계값을 정하기위해 상이동과 하이동을 나눈후에 한계값을 적용(상하로 안나눴을경우 내리다가 한계점이 발생하면 올릴수도 없게됨...) 
	if(ang3 < 0)//포신을 내릴때 음수
	if(turret1.transform.eulerAngles.x < 82)//포신을 아래로 내릴때 한계값
	turret1.transform.Rotate(Vector3.left * ang3 * amtToRot);
	
	if(ang3 >= 0)//포신을 올릴때 양수
	if(turret1.transform.eulerAngles.x > 30)//포신을 올릴때 한계값
	turret1.transform.Rotate(Vector3.left * ang3 * amtToRot);
	
//	Debug.Log(turret.transform.eulerAngles.y);
	//포탄 발사 -- 추가
	
	if(Input.GetButtonDown("Fire1")){
		var spPoint = GameObject.Find("spawnPoint");
		
		Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
		AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
		
		
		var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);		
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
	}
	

}