﻿#pragma strict
var snd : AudioClip;
var explosion : Transform;

function Start () {

}

function Update () {

}

function OnTriggerEnter (coll : Collider){
	Destroy(gameObject);
	Instantiate(explosion, transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);
	
	
	if (coll.gameObject.tag == "WALL"){
//	Instantiate(explosion, coll.transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);	
	Destroy(coll.gameObject);
	}	
	
	else if (coll.gameObject.tag == "ENEMY"){
		jsScore.hit++;
//		Debug.Log(coll.gameObject);
		if (jsScore.hit > 9) {
		Destroy(coll.transform.root.gameObject); // 최상위 오브잭트 파괴		
		//승리 화면으로 분기
		Application.LoadLevel("WinGame");
		}
		
	}	
	
	else if (coll.gameObject.tag == "TANK") {
	jsScore.lose++;
	if (jsScore.lose > 9) {
		//패배화면으로 분기
	Application.LoadLevel("LostGame");
		}
	}
}